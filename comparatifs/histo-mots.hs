import Data.List
import System.IO
import System.Process
import System.Environment

lettresbepo :: String
lettresbepo = "tesirunadpqlovéc'.,fbmgxjhàêèyzkçw  ù"

lettresazer :: String
lettresazer = "jfkdlsmqie,oruzhnvg!aù;cp:wytxêb*$àèé"

tapable :: String -> String -> Bool
tapable = all . flip elem

histogramme :: String -> [String] -> [Int]
histogramme touches mots =
    flip map (inits touches) $ \ts ->
        length . filter (tapable ts) $ mots 

writeHisto :: Handle -> [Int] -> IO ()
writeHisto f = hPutStr f . unlines . map show

plot :: FilePath -> FilePath -> FilePath -> String
plot fileb filea outfile = unlines
         ["set term png font 'arial,11' size 600,400;\n",
          "set output \"" ++ outfile ++ "\";\n",
          "set logscale;\n",
          "set key outside;\n",
         "set title \"nombre de mots qu\\'il est possible de taper avec les x touches les plus accessibles du clavier\";\n",
         "plot [1:36] " ++
         "\"" ++ fileb ++ "\" title 'bepo' with filledcurve,\\\n" ++
         "\"" ++ filea ++ "\" title 'azerty' with filledcurve;\n",
         "exit;\n"]


main :: IO ()
main = do
  [outfile] <- getArgs
  dict <- readFile "/usr/share/dict/french"
  let mots = lines dict
  (filea,fda) <- openTempFile "/tmp" "hist.aze"
  (fileb,fdb) <- openTempFile "/tmp" "hist.bep"
  writeHisto fdb $ histogramme lettresbepo mots
  writeHisto fda $ histogramme lettresazer mots
  (fileplot,fdplot) <- openTempFile "/tmp" "hist.plot"
  hPutStr fdplot $ plot fileb filea outfile
  mapM hClose [fda,fdb,fdplot]
  runCommand $ "gnuplot " ++ fileplot
  return ()
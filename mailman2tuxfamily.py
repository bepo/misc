#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Transform archives from mailman in a format usable by tuxfamily
#
# Copyright (C) 2008 Gaëtan Lehmann <gaetan.lehmann@jouy.inra.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#

import mailbox, sys, os, time, re

months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

def mkdir_p(path_list):
  path = ""
  for d in path_list:
    path += d + os.sep
    if not os.path.exists(path):
      os.mkdir(path)

def date(s):
  try:
    t = list(time.strptime(s, "%a %b %d %H:%M:%S %Y")[:3])
  except ValueError:
    try:
      t = time.strptime(" ".join(s.split()[:-1]), "%a, %d %b %Y %H:%M:%S")
    except ValueError:
      try:
        t = time.strptime(" ".join(s.split()[:-1]), "%d %b %Y %H:%M:%S")
      except ValueError:
          t = time.strptime(" ".join(s.split()[:-2]), "%a, %d %b %Y %H:%M:%S")
  year = str(t[0])
  month = str(t[1]).zfill(2)
  day = str(t[2]).zfill(2)
  return (year, month, day)
  
  
def convert(fname):
  print "processing:", fname
  
  fyear = "?"
  fmonth = "?"
  match = re.search(r'(\d*)-(\w*)\.txt', fname)
  if match:
    fyear = match.group(1)
    fmonth = str(months.index(match.group(2))+1).zfill(2)
  
  mbox = mailbox.mbox(fname)
  for k, msg in mbox.iteritems():
  
    d =msg["Date"]
    if not d:
      continue
      
    (year, month, day) = date(d)
    path = os.sep.join(["tuxfamily", year, month, day, msg["Message-id"][1:-1]])

    # some checks    
    if year != fyear:
      print "Warning: year in message (%s) doesn't match year in file name (%s) in date: %s." % (year, fyear, msg["Date"])
    if month != fmonth:
      print "Warning: month in message (%s) doesn't match month in file name (%s) in date %s." % (month, fmonth, msg["Date"])
    if os.path.exists(path):
      print "Warning: %s already exists." % path

    mkdir_p(["tuxfamily", year, month, day])
#     print " ", path
    f = file( path, "w")
    f.write(mbox.get_string(k))
    f.close()
    
    
if __name__ == "__main__":
  for fname in sys.argv[1:]:
    convert(fname)
